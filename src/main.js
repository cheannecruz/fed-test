// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import en     	   from './locale/en.json'
import pt  		     from './locale/pt.json'
import ja    	     from './locale/ja.json'
import Vue         from 'vue'
import App         from './App'
import router      from './router'
import Icon        from 'vue-svg-icon/Icon.vue'
import VueResource from 'vue-resource'
import VueI18n     from 'vue-i18n'
//import scss from 'scss';

Vue.component('icon', Icon);
Vue.use(VueResource)
Vue.use(VueI18n)
Vue.config.productionTip = false

var messages = {
  en : en,
  pt : pt,
  ja : ja
}

var i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages
})

Vue.prototype.$locale = {
  change (lang) {
    i18n.locale = lang
  },
  current () {
    return i18n.locale
  }
}

/* eslint-disable no-new */
var vm = new Vue({
  el: '#app',
  i18n,
  router,
  template: '<App/>',
  components: { App }
})