import Vue from 'vue'
import Router from 'vue-router'
import style_guide from '@/components/Style_guide'
import Flow from '@/components/Flow'
import Home from '@/components/Home'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: '',
      component: Home
    },
    {
      path: '/style_guide',
      name: 'style_guide',
      component: style_guide
    },
    {
      path: '/flow/:flow_id',
      name: 'Flow',
      component: Flow
    }
  ]
})
